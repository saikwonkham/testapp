<?php
require "PhpOrient/vendor/autoload.php";
use PhpOrient\PhpOrient;
require "db_connection.php";
session_start();

function image_upload($image)
	{
		if(isset($_FILES[$image]))
		{
			$errors=array();
			$file_name=$_FILES[$image]['name'];
			$file_size=$_FILES[$image]['size'];
			$file_temp=$_FILES[$image]['tmp_name'];
			$file_type=$_FILES[$image]['type'];
			$file_ext=strtolower(end(explode('.',$_FILES[$image]['name'])));
			$expensions=array("jpeg","jpg","png");
			if(in_array($file_ext, $expensions)===false){
				$errors[]="exteension not allowed, please choose a JPEG or PNG file";
			}

			if(empty($errors)==true){
				move_uploaded_file($file_temp, "images/".$file_name);
				echo "success";
			}
			else {
				print_r($errors);
			}
			//var_dump($file_name);die();
			return $file_name;
			//die();

	}
}
	


if(isset($_POST['insert']))
	{
		$address=$_POST['address'];
		$gender=$_POST['gender'];
		$day=$_POST['day'];
		$month=$_POST['month'];
		$year=$_POST['year'];
		$social_media=$_POST['social_media'];
		$nationality=$_POST['nationality'];
		$hobby=$_POST['hobby'];
		$body_type=$_POST['body_type'];
		$feet=$_POST['feet'];
		$inch=$_POST['inch'];
		$skin_color=$_POST['skin_color'];
		$faith=$_POST['faith'];
		$smoke=$_POST['smoke'];
		$drink=$_POST['drink'];
		$image=image_upload('image');
		$hair_color=$_POST['hair_color'];
		$rid=$_SESSION['rid'];
	}
$people = $client->query( "select * from People where @rid='$rid'");
foreach ($people as $key => $value) {
	$name=$value["name"];
	$email=$value["email"];
 	$password=$value["password"];
}

$record = $client->query( "select from People where @rid = '$rid'" )[0];
$_recUp = [ 'name'			=>$name,
			'email'			=>$email,
			'password'		=>$password,
			'address' 		=>$address,
			'gender' 		=> $gender, 
			'day' 			=> $day,
			'month'			=>$month,
			'year'			=>$year,
			'social_media'	=>$social_media,
			'nationality'	=>$nationality,
			'hobby'			=>$hobby,
			'body_type'		=>$body_type,
			'feet'			=>$feet,
			'inch'			=>$inch,
			'skin_color'	=>$skin_color,
			'faith'			=>$faith,
			'smoke'			=>$smoke,
			'drink'			=>$drink,
			'image'			=>$image,
			'hair_color'	=>$hair_color];
$recUp = $record->setOData( $_recUp );
$updated = $client->recordUpdate( $recUp );

$matching_people=$client->command("MATCH {class: People, as: people, 
									where: (name = $name 
											OR address = $address
											OR gender = $gender
											OR day = $day
											AND month = $month
											AND year = $year
											OR nationality = $nationality
											AND hobby = $hobby
											OR body_type = $body_type
											OR skin_color = $skin_color
											OR feet = $feet
											AND inch = $inch
											OR faith = $faith
											AND smoke = $smoke
											AND drink = $drink)} RETURN people");

 
 foreach ($matching_people as $key => $value) {
 		 $str=json_decode($value,true);
 		//  echo "<pre>";
		 // var_dump($str);
		 // echo "</pre>";
		
		$rid_other=$str["oData"]["people"];
		$client->command("CREATE EDGE FROM $rid TO $rid_other");
		$client->command("CREATE EDGE FROM $rid_other TO $rid");

}


 // $client->command( "insert into People set address='$address',
 // 	gender='$gender',
 // 	day='$day',
 // 	month='$month',
 // 	year='$year',
 // 	social_media='$social_media',
 // 	nationality='$nationality',
 // 	hobby='$hobby',
 // 	body_type='$body_type',
 // 	feet='$feet',
 // 	inch='$inch',
 // 	skin_color='$skin_color',
 // 	faith='$faith',
 // 	smoke='$smoke',
 // 	drink='$drink',
 // 	image='$image',
 // 	hair_color='$hair_color'
 // 	");

header("Location:profile.php"); 
?> 
