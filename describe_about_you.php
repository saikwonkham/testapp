<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap text limit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!--Google webfonts-->
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <style type="text/css">
      body {
    background:url(../img/debut_light.png) fixed;
    font-family: 'Roboto', sans-serif;
    color:#fff;
    }
    .box {
        color:#000;
        margin:150px auto;
        padding:20px;
        width:500px;
        height:240px;
        background:#fff;
        border-radius:3px;
        border-bottom:4px solid #5e95cd;
        box-shadow: 0px 0px 30px #888888;
    }
    .overlimit{color: red;}
    </style>
    <script type="text/javascript">

    </script>
  </head>
  <body>
  <section class="box">
    <h3>What's up?</h3>
    <textarea id="status" rows="5" style="width:485px"></textarea>
    <span id="text_counter"></span><input class="btn btn-large btn-primary pull-right" type="submit" id="posting" value="Post" />
  </section>
    
</body>
</html>