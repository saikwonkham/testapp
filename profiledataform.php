<!-- Kyaw Myo Htet -->
<!DOCTYPE html>
<html>
<head>
	<title>Register</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="template/css/bootstrap.min.css">

	<script type="text/javascript" src="template/js/jquery.js"></script>
	<script type="text/javascript" src="template/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="template/js/materialize.min.js"></script>

	<style type="text/css">
		body{
			background-image: url("images/aa.gif");
			-moz-background-size: cover;
			-webkit-background-size: cover;
			background-size: cover;
			background-position: top center !important;
			background-repeat: no-repeat !important;
			background-attachment: fixed;
		}
	</style>

</head>
<body>
<!-- profile picture -->
	<div class="row form-group">
		<form class="form-horizontal" role="form" method="POST" action="saveprofiledata.php" enctype="multipart/form-data">
			<div class="form-group">
    			
  			</div>
		</form>
	</div>
<!-- user's biography -->
	<div class="row form-group">
		<form class="form-horizontal" role="form" method="POST" action="saveprofiledata.php" enctype="multipart/form-data">

  			<div class="form-group">
    			<label for="address" class="col-sm-2 control-label" >City</label>
    				<div class="col-sm-8">
      					<input type="text" style="border-radius:10px;padding:10px;width:100%;height:37px;" required id="address" name="address" placeholder="Enter your address">
    				</div>
  			</div>

<input type="hidden" value="rid" name='rid'>

  			<div class="form-group">
    			<label for="gender" class="col-sm-2 control-label" >Gender</label>
	    			<div class="col-sm-8">
	      				<label class="radio-inline">
	      					<input type="radio" required id="male" name="gender" value="Male">Male
	    				</label>
		    			<label class="radio-inline">
		      				<input type="radio" required id="female" name="gender" value="Female">Female
		    			</label>
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="birth_date" class="col-sm-2 control-label" >Date of Birth</label>
    				<div class="col-sm-2">
      					<select class="form-control" id="day" name="day">
					        <option value="1">1</option>
	                        <option value="2">2</option>
	                        <option value="3">3</option>
	                        <option value="4">4</option>
	                        <option value="5">5</option>
	                        <option value="6">6</option>
	                        <option value="7">7</option>
	                        <option value="8">8</option>
	                        <option value="9">9</option>
	                        <option value="10">10</option>
	                        <option value="11">11</option>
	                        <option value="12">12</option>
	                        <option value="13">13</option>
	                        <option value="14">14</option>
	                        <option value="15">15</option>
	                        <option value="16">16</option>
	                        <option value="17">17</option>
	                        <option value="18">18</option>
	                        <option value="19">19</option>
	                        <option value="20">20</option>
	                        <option value="21">21</option>
	                        <option value="22">22</option>
	                        <option value="23">23</option>
	                        <option value="24">24</option>
	                        <option value="25">25</option>
	                        <option value="26">26</option>
	                        <option value="27">27</option>
	                        <option value="28">28</option>
	                        <option value="29">29</option>
	                        <option value="30">30</option>
	                        <option value="31">31</option>
					    </select>
    				</div>
    				<div class="col-sm-2">
      					<select class="form-control" id="month" name="month">
					        <option value="1">Jan</option>
	                        <option value="2">Feb</option>
	                        <option value="3">Mar</option>
	                        <option value="4">Apr</option>
	                        <option value="5">May</option>
	                        <option value="6">Jun</option>
	                        <option value="7">Jul</option>
	                        <option value="8">Aug</option>
	                        <option value="9">Sep</option>
	                        <option value="10">Oct</option>
	                        <option value="11">Nov</option>
	                        <option value="12">Dec</option>
					    </select>
    				</div>
    				<div class="col-sm-2">
      					<select class="form-control" id="year" name="year">
					        <option value="1960">1960</option>
	                        <option value="1961">1961</option>
	                        <option value="1962">1962</option>
	                        <option value="1963">1963</option>
	                        <option value="1964">1964</option>
	                        <option value="1965">1965</option>
	                        <option value="1966">1966</option>
	                        <option value="1967">1967</option>
	                        <option value="1968">1968</option>
	                        <option value="1969">1969</option>
	                        <option value="1970">1970</option>
	                        <option value="1971">1971</option>
	                        <option value="1972">1972</option>
	                        <option value="1973">1973</option>
	                        <option value="1974">1974</option>
	                        <option value="1975">1975</option>
	                        <option value="1976">1976</option>
	                        <option value="1977">1977</option>
	                        <option value="1978">1978</option>
	                        <option value="1979">1979</option>
	                        <option value="1980">1980</option>
	                        <option value="1981">1981</option>
	                        <option value="1982">1982</option>
	                        <option value="1983">1983</option>
	                        <option value="1984">1984</option>
	                        <option value="1985">1985</option>
	                        <option value="1986">1986</option>
	                        <option value="1987">1987</option>
	                        <option value="1988">1988</option>
	                        <option value="1989">1989</option>
	                        <option value="1990">1990</option>
	                        <option value="1991">1991</option>
	                        <option value="1992">1992</option>
	                        <option value="1993">1993</option>
	                        <option value="1994">1994</option>
	                        <option value="1995">1995</option>
	                        <option value="1996">1996</option>
	                        <option value="1997">1997</option>
	                        <option value="1998">1998</option>
	                        <option value="1999">1999</option>
	                        <option value="2000">2000</option>
					    </select>
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="social_media" class="col-sm-2 control-label" >Social Media</label>
    				<div class="col-sm-8">
      					<input type="text" style="border-radius:10px;padding:10px;width:100%;height:37px;" required id="social_media" name="social_media" placeholder="Social Media link">
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="nationality" class="col-sm-2 control-label" >Nationality</label>
    				<div class="col-sm-8">
      					<input type="text" style="border-radius:10px;padding:10px;width:100%;height:37px;" required id="nationality" name="nationality" placeholder="Nationality">
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="hobby" class="col-sm-2 control-label" >Hobby</label>
    				<div class="col-sm-8">
      					<input type="text" style="border-radius:10px;padding:10px;width:100%;height:37px;" required id="hobby" name="hobby" placeholder="Enter your Hobby">
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="body_type" class="col-sm-2 control-label" >Body Type</label>
    				<div class="col-sm-2">
      					<select class="form-control" id="body_type" name="body_type">
      						<option value="slim">Slim</option>
      						<option value="fat">Fat</option>
      					</select>
    				</div>
  			</div>


  			<div class="form-group">
    			<label for="skin_color" class="col-sm-2 control-label" >Skin Color</label>
    				<div class="col-sm-2">
      					<select class="form-control" id="skin_color" name="skin_color">
      						<option value="white">White</option>
      						<option value="brown">Brown</option>
      						<option value="dark_brown">Dark Brown</option>
      					</select>
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="hair_color" class="col-sm-2 control-label" >Hair Color</label>
    				<div class="col-sm-2">
      					<select class="form-control" id="hair_color" name="hair_color">
      						<option value="white">White</option>
      						<option value="brown">Brown</option>
      						<option value="dark_brown">Dark Brown</option>
      					</select>
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="height" class="col-sm-2 control-label" >Height</label>
    				<div class="col-sm-1">
      					<select class="form-control" id="feet" name="feet">
      						<option value="4">4'</option>
      						<option value="5">5'</option>
      						<option value="6">6'</option>
      						<option value="7">7'</option>
      					</select>
    				</div>
    				<div class="col-sm-1">
      					<select class="form-control" id="inch" name="inch">
      						<option value="0">0"</option>
      						<option value="1">1"</option>
      						<option value="2">2"</option>
      						<option value="3">3"</option>
      						<option value="4">4"</option>
      						<option value="5">5"</option>
      						<option value="6">6"</option>
      						<option value="7">7"</option>
      						<option value="8">8"</option>
      						<option value="9">9"</option>
      						<option value="10">10"</option>
      						<option value="11">11"</option>
      					</select>
    				</div>
  			</div>

 			<div class="form-group">
    			<label for="faith" class="col-sm-2 control-label" >Faith</label>
    				<div class="col-sm-8">
      					<input type="text" style="border-radius:10px;padding:10px;width:100%;height:37px;" required id="faith" name="faith" placeholder="Enter your Faith">
    				</div>
  			</div>

  			<div class="form-group">
    			<label for="smoke" class="col-sm-2 control-label" >Smoke</label>
    				<div class="col-sm-8">
	    				<label class="radio-inline">
		      				<input type="radio" required id="yes_smoke" name="smoke" value="Yes">Yes
		    			</label> 
			    		<label class="radio-inline">
			      			<input type="radio" required id="no_smoke" name="smoke" value="Np">No
			    		</label>
		    		</div>
  			</div>

  			<div class="form-group">
    			<label for="drink" class="col-sm-2 control-label" >Drink</label>
    				<div class="col-sm-8">
	    				<label class="radio-inline">
		      				<input type="radio" required id="yes_drink" name="drink" value="Yes">Yes
		    			</label>
			    		<label class="radio-inline">
			      			<input type="radio" required id="no_drink" name="drink" value="No">No
			    		</label>
			    	</div>
  			</div>

  			<div class="form-group">
				<label for="image" class="col-sm-2 control-label">Images</label>
				<div class="col-sm-8">
					<input name="image" style="border-radius:10px;padding:10px;width:100%;height:37px;" type="file" required id="image" placeholder="Choose Image">
				</div>  				
  			</div>

  			<div class="form-group">
            <!-- description 
            <label class="control-label col-md-2 col-sm-2 col-xs-2 col-lg-2">
              Describe about you<br/>
              <h7 style="font-size:10px;">Just have fun with it, don't think too much!</h7>
            </label>
            <div class="col-sm-8">
              <textarea class="form-control" name="description" >
                
              </textarea>
            </div>
             end description -->
          </div>

  			<div class="form-group">
    				<div class="col-sm-offset-2 col-sm-10">
      					<button type="submit" class="btn btn-info" name="insert">Sign up</button>
    				</div>
  			</div>
		</form>
	</div>
</body>
</html>


