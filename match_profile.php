
<?php
require("db_connection.php");
session_start();
$rid=$_SESSION['rid'];
$people = $client->query( "select * from People where @rid='$rid'");
//echo "<pre>";//int_r($people);echo "</pre>";
foreach ($people as $key => $value) {
	//echo "<pre>";
	//print_r($value["name"]);
	//echo "</pre>";
	$name=$value["name"];
	$address=$value["address"];
	$nationality=$value["nationality"];
	$hobby=$value["hobby"];
	$date_of_birth=$value["year"];
	$body_type=$value["body_type"];
	$skin_color=$value["skin_color"];
	$hair_color=$value["hair_color"];
	$feet=$value["feet"];
	$inch=$value["inch"];
	$faith=$value["faith"];
	$smoke=$value["smoke"];
	$drink=$value["drink"];
	$image=$value["image"];

	$imgArray = explode(',', $image);
	$imgArr=array_reverse($imgArray);
	
	//print_r(sizeof($imgArray));die();
	
}
//die();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Profile</title>
		<!-- my css  start -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<link rel="stylesheet" type="text/css" href="template/css/bootstrap.min.css">

		<script type="text/javascript" src="template/js/jquery.js"></script>
		<script type="text/javascript" src="template/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="template/js/materialize.min.js"></script>

		<!-- my css  end -->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body id="top">

		<!-- Header -->

			<header id="header">
				<div class="inner">
					<a href="#" class="image avatar">
						<img src="images/<?php echo $imgArray[sizeof($imgArray)-1];?>" alt="" height="100" width="100" />
					</a>

					<?php //echo $image;?>

					<h5><strong>Name:<?php echo $name;?></strong></h45 >
					<h5><strong>City :<?php echo $address;?></strong></h5>
					<h5><strong>Nationality :<?php echo $nationality;?></strong></h5>
					<h5><strong>Hobby                       :<?php echo $hobby;?></strong></h5>
					<h5><strong>Born_On :<?php echo $date_of_birth;?></strong></h5>
					<h5><strong>Body Type:<?php echo $body_type;?></strong></h5
					<h5><strong>Skin Color:<?php echo $skin_color;?></strong></h5>
					<h5><strong>Hair Color:<?php echo $hair_color;?></strong></h5>
					<h5><strong>Heith :<?php echo $feet;?>' <?php echo $inch;?>"</strong></h5>
					<h5><strong>Faith:<?php echo $faith;?></strong></h5>
					<h5><strong>Smoke:<?php echo $drink;?></strong></h5>
					<h5><strong>Drink:<?php echo $drink;?></strong></h5>
				</div>
			</header>

		<!-- Main -->
			<div id="main">

				<!-- One -->
					<section id="one">
						<header class="major">
							<h2>Someone special<br />
							is waiting for you ...</h2>
						</header>
						<p>Accumsan orci faucibus id eu lorem semper. Eu ac iaculis ac nunc nisi lorem vulputate lorem neque cubilia ac in adipiscing in curae lobortis tortor primis integer massa adipiscing id nisi accumsan pellentesque commodo blandit enim arcu non at amet id arcu magna. Accumsan orci faucibus id eu lorem semper nunc nisi lorem vulputate lorem neque cubilia.</p>
					</section>

				<!-- Two -->
					<section id="two">
						<h2>Recent Work</h2>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<a href="images/fulls/01.jpg" class="image fit thumb"><img src="images/thumbs/01.jpg" alt="" /></a>
								<h3>Magna sed consequat tempus</h3>
								<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="images/fulls/02.jpg" class="image fit thumb"><img src="images/thumbs/02.jpg" alt="" /></a>
								<h3>Ultricies lacinia interdum</h3>
								<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
							</article>
							<article class="6u 12u$(xsmall) work-item">
								<a href="images/fulls/03.jpg" class="image fit thumb"><img src="images/thumbs/03.jpg" alt="" /></a>
								<h3>Tortor metus commodo</h3>
								<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="images/fulls/04.jpg" class="image fit thumb"><img src="images/thumbs/04.jpg" alt="" /></a>
								<h3>Quam neque phasellus</h3>
								<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
							</article>
							<article class="6u 12u$(xsmall) work-item">
								<a href="images/fulls/05.jpg" class="image fit thumb"><img src="images/thumbs/05.jpg" alt="" /></a>
								<h3>Nunc enim commodo aliquet</h3>
								<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="images/fulls/06.jpg" class="image fit thumb"><img src="images/thumbs/06.jpg" alt="" /></a>
								<h3>Risus ornare lacinia</h3>
								<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
							</article>
						</div>
						<ul class="actions">
							<li><a href="#" class="button">Full Portfolio</a></li>
						</ul>
					</section>

				<!-- Three -->
					<section id="three">
						<h2>Get In Touch</h2>
						<p>Aung Phyo Maw</p>
						<div class="row">
							<div class="8u 12u$(small)">
								<form method="post" action="#">
									<div class="row uniform 50%">
										<div class="6u 12u$(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
										<div class="6u$ 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
										<div class="12u$"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
									</div>
								</form>
								<ul class="actions">
									<li><input type="submit" value="Send Message" /></li>
								</ul>
							</div>
							<div class="4u$ 12u$(small)">
								<ul class="labeled-icons">
									<li>
										<h3 class="icon fa-home"><span class="label">Address</span></h3>
										1234 Somewhere Rd.<br />
										Nashville, TN 00000<br />
										United States
									</li>
									<li>
										<h3 class="icon fa-mobile"><span class="label">Phone</span></h3>
										000-000-0000
									</li>
									<li>
										<h3 class="icon fa-envelope-o"><span class="label">Email</span></h3>
										<a href="#">hello@untitled.tld</a>
									</li>
								</ul>
							</div>
						</div>
					</section>

				
						<section>
						<!-- start-->
							<div class="row form-group">
								<form class="form-horizontal" role="form" method="POST" action="update.php" enctype="multipart/form-data">
									<div class="form-group">
										<label for="image" class="col-sm-4 control-label"><h2>More Upload Image</h2></label>
										<div class="col-sm-5">
											<input name="subimage" style="border-radius:10px;padding:10px;width:100%;height:37px;" type="file" required id="subimage" placeholder="Choose Image">
										</div> 
										<div class="col-sm-3">
					      					<button type="submit" class="btn btn-info" name="upload">Upload</button>
					    				</div> 				
						  			</div>
							    </form>
							</div>

		<!-- start-->

							<div class="box alt">
								<div class="row 50% uniform">
								
									<div class="12u$"><span class="image fit"><img src="images/<?php echo $image;?>" alt="" /></span></div>
	
								<?php foreach ($imgArr as $img) 
								{//var_dump($img);
									?>
									<div class="6u"><span class="image fit"><img src="images/<?php echo $img;?>" alt="" /></span></div>
								<?php }?>
									
								</div>
							</div>
							
				

			</div> Four -->

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>